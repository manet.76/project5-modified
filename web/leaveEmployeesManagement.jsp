<%--
  Created by IntelliJ IDEA.
  User: ADAK-Shemroon
  Date: 11/1/2020
  Time: 2:51 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!doctype html>
<html>
<head>
    <title>Title</title>
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <link
            rel="stylesheet"
            href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css"
            integrity="sha384-vus3nQHTD+5mpDiZ4rkEPlnkcyTP+49BhJ4wJeJunw06ZAp+wzzeBPUXr42fi8If"
            crossorigin="anonymous">
    <jsp:include page="managerHeader.jsp"/>
    <script>
        function acceptLeave(leaveId) {
            window.location = '/managerController.do?action=acceptLeave&leaveId=' + leaveId;

        }

        function rejectLeave(leaveId) {
            if (confirm('<fmt:bundle basename="resource_fa"><fmt:message key="reject leave"/></fmt:bundle>')) {
                window.location = '/managerController.do?action=rejectLeave&leaveId=' + leaveId;
            }
        }
    </script>
</head>
<body dir="rtl">
<jsp:include page="body.jsp"/>
<c:choose>
<c:when test="${empty requestScope.beneathEmployee}">
    <div style="width: 400px;border-radius: 5px; margin: 10px auto;text-align: center;">
        <div class="alert alert-info" id="info-alert">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong><fmt:bundle basename="resource_fa">
                <fmt:message key="no leave request"/>
            </fmt:bundle>
            </strong>
        </div>
    </div>
</c:when>
<c:otherwise>
<div class="container" style="margin-top: 50px;border-radius: 6px;background-color: deepskyblue;">
    <table class="table table-bordered table-hover table-responsive-lg">
        <thead class="thead-light ">
        <tr style="border-radius: 10px;">
            <th class="text-center" scope="col"><fmt:bundle basename="resource_fa">
                <fmt:message key="firstName"/>
            </fmt:bundle></th>
            <th class="text-center" scope="col"><fmt:bundle basename="resource_fa">
                <fmt:message key="lastName"/>
            </fmt:bundle></th>
            <th class="text-center" scope="col"><fmt:bundle basename="resource_fa">
                <fmt:message key="leaveFromDate"/>
            </fmt:bundle></th>
            <th class="text-center" scope="col"><fmt:bundle basename="resource_fa">
                <fmt:message key="leaveToDate"/>
            </fmt:bundle></th>
            <th class="text-center" scope="col"><fmt:bundle basename="resource_fa">
                <fmt:message key="leave status"/>
            </fmt:bundle></th>
            <th class="text-center" scope="col"><fmt:bundle basename="resource_fa">
                <fmt:message key="action"/>
            </fmt:bundle></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${requestScope.beneathEmployee}" var="Employee">
            <c:forEach items="${Employee.leaveList}" var="leave">
                <tr>
                    <td hidden><c:out value="${leave.id}"/></td>
                    <td><c:out value="${Employee.firstName}"/></td>
                    <td><c:out value="${Employee.lastName}"/></td>
                    <td><c:out value="${leave.leaveFromDate}"/></td>
                    <td><c:out value="${leave.leaveToDate}"/></td>
                    <td><c:out value="${leave.leaveStatus.name}"/></td>
                    <td class="text-right" style="width: 21%;">
                        <button type="button"
                                class="btn btn-primary btn-rounded btn-lm my-0 badge-pill " value="update"
                                style="width: 82px;" onclick="acceptLeave(${leave.id})"><span
                                class="fa fa-check"> <fmt:bundle basename="resource_fa">
                            <fmt:message key="accept"/>
                        </fmt:bundle></span></button>
                        <button type="button"
                                class="btn btn-danger btn-rounded btn-lm my-0 badge-pill " value="delete"
                                style="width: 80px;margin-right:10px;" onclick="rejectLeave(${leave.id})"><span
                                class="fa fa-times"> <fmt:bundle basename="resource_fa">
                            <fmt:message key="reject"/>
                        </fmt:bundle> </span></button>
                    </td>
                </tr>
            </c:forEach>
        </c:forEach>
        </tbody>
    </table>
    </c:otherwise>
    </c:choose>
</div>
</body>
</html>
