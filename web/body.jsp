<%--
  Created by IntelliJ IDEA.
  User: ADAK-Shemroon
  Date: 10/21/2020
  Time: 9:56 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <style>
        html {
            width: 100%;
            height: 150%;
            overflow: hidden;
            overflow-y: scroll;

        }

        body {
            margin: 0;
            padding: 0;
            overflow-: auto;
            font-family: 'Open Sans', sans-serif;
            background: linear-gradient(
                    limegreen,
                    transparent
            ),
            linear-gradient(
                    90deg,
                    skyblue,
                    transparent
            ),
            linear-gradient(
                    -90deg,
                    coral,
                    transparent
            );
            background-blend-mode: screen;
            background-repeat: no-repeat;
        }

        .container {
            background: linear-gradient(
                    limegreen,
                    transparent
            ),
            linear-gradient(
                    90deg,
                    skyblue,
                    transparent
            ),
            linear-gradient(
                    -90deg,
                    coral,
                    transparent
            );
            background-blend-mode: screen;
        }
    </style>
    <meta charset="utf-8">
</head>
<body>

</body>
</html>
