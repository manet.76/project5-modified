package common.service;

import common.repository.CategoryElementDao;
import model.entity.CategoryElement;

public class CategoryElementService {
    private static CategoryElementService categoryElementService = new CategoryElementService();

    public static CategoryElementService getInstance() {
        return categoryElementService;
    }

    private CategoryElementService() {
    }
    public CategoryElement findByCodeCategory(String code){
        CategoryElementDao categoryElementDao=new CategoryElementDao();
        return categoryElementDao.findByCode(code);
    }
}
