package common.service;

import common.repository.LeaveEmployeeDao;
import model.entity.LeaveEmployee;

import java.time.LocalDate;

public class LeaveEmployeeService {
    private static LeaveEmployeeService leaveService = new LeaveEmployeeService();

    private LeaveEmployeeService() {
    }

    public static LeaveEmployeeService getInstance() {
        return leaveService;
    }

    public void leaveEmployeeSave(LeaveEmployee leaveEmployee) {
        LeaveEmployeeDao leaveEmployeeDao = new LeaveEmployeeDao();
        leaveEmployeeDao.save(leaveEmployee);
    }

    public void changeLeaveStatusToAccepted(int leaveId , LocalDate lastModifyDataTime) {
        LeaveEmployeeDao leaveEmployeeDao = new LeaveEmployeeDao();
        leaveEmployeeDao.changeStatusToAccepted(leaveId , lastModifyDataTime);
    }

    public void changeLeaveStatusToRejected(int leaveId , LocalDate lastModifyDataTime) {
        LeaveEmployeeDao leaveEmployeeDao = new LeaveEmployeeDao();
        leaveEmployeeDao.changeStatusToRejected(leaveId , lastModifyDataTime);
    }
}
