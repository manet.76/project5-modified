package common.service;

import common.repository.ManagerDao;
import model.entity.Employee;

import java.util.List;

public class ManagerService {

    private static ManagerService managerService = new ManagerService();

    public static ManagerService getInstance() {
        return managerService;
    }

    private ManagerService() {
    }

    public  Employee login(String username, String password) {
        ManagerDao managerDao = new ManagerDao();
        Employee employee = managerDao.validate(username, password);
        return employee;
    }
    public void saveEmployee(Employee employee){
        ManagerDao managerDao =new ManagerDao();
        managerDao.save(employee);

    }
    public List<Employee> findAllEmployee(){
        ManagerDao managerDao =new ManagerDao();
        return managerDao.findAll();
    }
    public void updateEmployee(Employee employee){
        ManagerDao managerDao =new ManagerDao();
        managerDao.update(employee);
    }

    public List<Employee> getAllManagerEmployee(){
        ManagerDao managerDao = new ManagerDao();
        return managerDao.getAllManager();
    }
    public Employee getSelectedEmployeeManager(String firstName , String lastName){
        ManagerDao managerDao =new ManagerDao();
        return managerDao.getSelectedManager(firstName,lastName);
    }
    public void inactiveEmployee(int employeeId){
        ManagerDao managerDao = new ManagerDao();
        managerDao.inactive(employeeId);
    }
    public List<Employee> searchEmployee(Employee employee){
        ManagerDao managerDao=new ManagerDao();
        return managerDao.search(employee);
    }
    public Employee findEmployeeById(int id){
        ManagerDao managerDao = new ManagerDao();
        return managerDao.findById(id);
    }
    public List<String> findAllUsernameEmployee(){
        ManagerDao managerDao = new ManagerDao();
        return managerDao.findAllUsername();
    }
    public List<Employee> findAllBeneathEmployee(Employee manager){
        ManagerDao managerDao = new ManagerDao();
        return managerDao.findAllBeneathEmployee(manager);
    }
    public Employee findManagetByUsername(String username){
        ManagerDao managerDao = new ManagerDao();
        return managerDao.findByUsername(username);
    }

}
