package common.repository;

import common.HibernateUtil;
import model.entity.Email;
import model.entity.Employee;
import model.entity.LeaveEmployee;
import org.hibernate.MultiIdentifierLoadAccess;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDao {
    public Employee findByUsername(String username) {
        Employee employee = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            Query query = session.createQuery("select e from Employee e where e.username =: username");
            query.setParameter("username", username);
            employee = (Employee) query.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return employee;
    }

    public void updateEmployee(Employee employee) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            Employee updatedEmployee = session.get(Employee.class, employee.getId());
            updatedEmployee.setFirstName(employee.getFirstName());
            updatedEmployee.setLastName(employee.getLastName());
            updatedEmployee.setFatherName(employee.getFatherName());
            updatedEmployee.setEmail(employee.getEmail());
            updatedEmployee.setLastModifyDataTime(employee.getLastModifyDataTime());
            session.update(updatedEmployee);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public void updateLeaveEmployee(Employee employee, LeaveEmployee leaveEmployee) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            Employee updatedEmployee = session.get(Employee.class, employee.getId());
            updatedEmployee.getLeaveList().add(leaveEmployee);
            session.update(updatedEmployee);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public List<LeaveEmployee> findLeaveEmployee(Employee employee) {
        List<LeaveEmployee> leaveEmployeeSet = new ArrayList<>();
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            Query query = session.createQuery("select e.leaveList from Employee e where e.id =: id");
            query.setParameter("id", employee.getId());
            leaveEmployeeSet = (List<LeaveEmployee>) query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return leaveEmployeeSet;
    }

    public List<Employee> getAllEmployees() {
        List<Employee> employeeList = new ArrayList<>();
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            Query query = session.createQuery("select e from Employee e ");
            employeeList = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return employeeList;
    }

    public Employee findById(int id) {
        Employee employee = new Employee();
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            Query query = session.createQuery("select e from Employee e where e.id =:id");
            query.setParameter("id", id);
            employee = (Employee) query.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return employee;
    }

    public void updateSentEmail(Employee employee, int emailId) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            Query query = session.createQuery("select email from  Email  email  where email.id =:emailId");
            query.setParameter("emailId", emailId);
            Email email = (Email) query.getSingleResult();
            Employee updatedEmployee = session.get(Employee.class, employee.getId());
            updatedEmployee.getSentEmails().add(email);
            session.update(updatedEmployee);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public List<Employee> receivedEmailEmployees(List<Integer> employeeIds) {
        List<Employee> receivedEmailEmployees = new ArrayList<>();
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            MultiIdentifierLoadAccess<Employee> multiLoadAccess = session.byMultipleIds(Employee.class);
            receivedEmailEmployees = multiLoadAccess.multiLoad(employeeIds);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return receivedEmailEmployees;
    }

}

