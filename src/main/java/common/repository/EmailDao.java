package common.repository;

import common.HibernateUtil;
import model.entity.Email;
import model.entity.Employee;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class EmailDao {
    public int save(Email email) {
        Transaction transaction = null;
        Integer emailId = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            emailId = (Integer) session.save(email);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return emailId;
    }

    public List<Email> receivedEmails(Employee employee) {
        List<Email> receivedEmails = new ArrayList<>();
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            Query query = session.createQuery("select  " +
                    "email from Email email join email.receiverEmployees employee  " +
                    "  where employee.id =: id");
            query.setParameter("id", employee.getId());
            receivedEmails = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return receivedEmails;
    }

    public List<Object[]> receivedEmailsInfo(Employee employee) {
        List<Object[]> receivedEmailsInfo = new ArrayList<>();
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            Query query = session.createQuery("select email.id , email.creationDataTime," +
                    " sender.firstName , sender.lastName , email.content , email.filePath from Email email , Employee sender join email.receiverEmployees receiver join sender.sentEmails se  " +
                    "  where receiver.id =: id AND se.id = email.id");
            query.setParameter("id", employee.getId());
            receivedEmailsInfo = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return receivedEmailsInfo;
    }
    public List<Object[]> sentEmailsInfo(Employee employee) {
        List<Object[]> sentEmailsInfo = new ArrayList<>();
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            Query query = session.createQuery("select email.id ,email.creationDataTime ,receiver.firstName,receiver.lastName, " +
                    "email.content , email.filePath from Employee sender,Employee receiver, Email email join sender.sentEmails sentEmails join sentEmails.receiverEmployees re " +
                    "where sender.id =:id and sentEmails.id=email.id   AND re.id = receiver.id");
            query.setParameter("id", employee.getId());
            sentEmailsInfo = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sentEmailsInfo;
    }
}
