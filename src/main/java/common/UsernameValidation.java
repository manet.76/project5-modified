package common;

import common.service.ManagerService;

import java.util.List;

public class UsernameValidation {

    public boolean duplicateUsername(String employeeUsername){
        boolean invalidUsername = false;
        List<String> allSavedUsernames= ManagerService.getInstance().findAllUsernameEmployee();
        for (String username : allSavedUsernames)
        {
            if(employeeUsername.equals(username)){
                invalidUsername = true;
            }
        }
        return invalidUsername;
    }
}
