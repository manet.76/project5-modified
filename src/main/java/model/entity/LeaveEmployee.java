package model.entity;


import javax.persistence.Entity;
import javax.persistence.*;
import java.time.LocalDate;

@Entity(name = "LeaveEmployee")
@Table(name = "t_LeaveEmployee")
public class LeaveEmployee extends model.entity.Entity {


    @Column(name = "c_leaveToDate", columnDefinition = "DATE")
    private LocalDate leaveToDate;


    @Column(name = "c_leaveFromDate", columnDefinition = "DATE")
    private LocalDate leaveFromDate;

    @ManyToOne()
    @JoinColumn(name = "c_leaveStatus")
    private CategoryElement leaveStatus;

    public LeaveEmployee(LocalDate leaveFromDate, LocalDate leaveToDate, CategoryElement leaveStatus, LocalDate creationDataTime) {
        this.leaveFromDate = leaveFromDate;
        this.leaveToDate = leaveToDate;
        this.leaveStatus = leaveStatus;
        this.creationDataTime = creationDataTime;
    }

    public LeaveEmployee() {

    }

    public LocalDate getLeaveToDate() {
        return leaveToDate;
    }

    public void setLeaveToDate(LocalDate leaveToDate) {
        this.leaveToDate = leaveToDate;
    }

    public LocalDate getLeaveFromDate() {
        return leaveFromDate;
    }

    public void setLeaveFromDate(LocalDate leaveFromDate) {
        this.leaveFromDate = leaveFromDate;
    }

    public CategoryElement getLeaveStatus() {
        return leaveStatus;
    }

    public void setLeaveStatus(CategoryElement leaveStatus) {
        this.leaveStatus = leaveStatus;
    }
}
