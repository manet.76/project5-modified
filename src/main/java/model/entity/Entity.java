package model.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@MappedSuperclass
public class Entity implements Serializable
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO )
    @Column(columnDefinition = "INTEGER",  nullable = false , unique = true)
    public int id;


    @Column(name = "c_creationDataTime" , columnDefinition = "DATE")
    public LocalDate creationDataTime;


    @Column(name = "c_lastModifyDataTime" , columnDefinition = "DATE")
    public LocalDate lastModifyDataTime;

    public LocalDate getCreationDataTime() {
        return creationDataTime;
    }

    public void setCreationDataTime(LocalDate creationDataTime) {
        this.creationDataTime = creationDataTime;
    }

    public LocalDate getLastModifyDataTime() {
        return lastModifyDataTime;
    }

    public void setLastModifyDataTime(LocalDate lastModifyDataTime) {
        this.lastModifyDataTime = lastModifyDataTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
