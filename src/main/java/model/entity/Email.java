package model.entity;

import javax.persistence.Entity;
import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "Email")
@Table(name = "t_Email")
public class Email extends model.entity.Entity {

    @Column(name = "c_content",columnDefinition = "VARCHAR(255)")
    private String content;

    @Column(name = "c_filePath" ,columnDefinition = "VARCHAR(255)")
    private String filePath;

    @OneToMany()
    @JoinTable(name = "m_Employee_Email" ,
            joinColumns = {@JoinColumn(name = "c_receivedEmailId" )},
    inverseJoinColumns = {@JoinColumn(name = "c_emailReceiverId" )})
    private Set<Employee> receiverEmployees = new HashSet<Employee>();



    public Email(String content , Set<Employee> employees , LocalDate creationDataTime){
        this.content = content;
        this.receiverEmployees = employees;
        this.creationDataTime = creationDataTime;
    }

    public Email() {
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Set<Employee> getReceiverEmployees() {
        return receiverEmployees;
    }

    public void setReceiverEmployees(Set<Employee> receiverEmployees) {
        this.receiverEmployees = receiverEmployees;
    }
}
