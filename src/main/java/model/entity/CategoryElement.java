package model.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity(name = "CategoryElement")
@Table(name = "t_CategoryElement")
public class CategoryElement extends model.entity.Entity {
    @Column(name = "c_name",columnDefinition = "VARCHAR(255)")
    private String name;

    @Column(name = "c_code",columnDefinition = "VARCHAR(255)")
    private String code;


    public CategoryElement() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
