package model.entity;

import javax.persistence.Entity;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Entity(name = "Category")
@Table(name = "t_Category")
public class Category extends model.entity.Entity {

    @Column(name = "c_item",columnDefinition = "VARCHAR(255)")
    private String item;

    @OneToMany()
    @JoinColumn(name = "c_categoryElementList")
    private Set<CategoryElement> categoryElementList = new HashSet<CategoryElement>();


    public Category() {
    }



    public Set<CategoryElement> getCategoryElementList() {
        return categoryElementList;
    }

    public void setCategoryElementList(Set<CategoryElement> categoryElementList) {
        this.categoryElementList = categoryElementList;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }
}
